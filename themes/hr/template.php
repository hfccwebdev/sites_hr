<?php

/**
 * @file
 * Custom theme functions for HR website.
 */

/**
 * Implements template_preprocess_node().
 */
function hr_preprocess_node(&$variables) {
  $variables['content']['job_bids'] = array(
    '#markup' => views_embed_view('job_bids', 'available_jobs_block', $variables['nid']),
    '#weight' => 99,
  );
  $variables['content']['payroll_dates'] = array(
    '#markup' => views_embed_view('upcoming_pay_dates', 'upcoming_dates_block', $variables['nid']),
  );
}
