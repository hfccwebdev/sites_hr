<?php

/**
 * @file
 * Interface and Controller for PayCycle entities.
 */

/**
 * PayCycle class.
 */
class PayCycle extends Entity {
  protected function defaultLabel() {
    //todo: Filter all of these into displayable values.
    return t('@cycle Pay Cycle for @group on @date', array(
      '@cycle' => !empty($this->pay_cycle) ? paycycle_cycle_opts($this->pay_cycle) : 'ERROR',
      '@group' => !empty($this->pay_group) ? paycycle_group_opts($this->pay_group) : 'ERROR',
      '@date' => !empty($this->pay_date) ? format_date($this->pay_date, 'custom', 'n/j/Y') : 'ERROR',
    ));
  }
  protected function defaultUri() {
    return array('path' => 'admin/structure/paycycle/' . $this->identifier());
  }
}

/**
 * PayCycleController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class PayCycleController extends EntityAPIController {

  /**
   * Create and return a new entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'pay_cycle' => NULL,
      'pay_group' => NULL,
      'pay_date' => NULL,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content[] = array(
      '#type' => 'item',
      '#field_name' => 'paycycle_pay_date',
      '#label' => t('Pay date'),
      '#label_display' => 'inline',
      '#markup' => !empty($entity->pay_date) ? format_date($entity->pay_date, 'custom', 'n/j/Y') : t('ERROR'),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    $content[] = array(
      '#type' => 'item',
      '#field_name' => 'paycycle_pay_cycle',
      '#label' => t('Pay cycle'),
      '#label_display' => 'inline',
      '#markup' => !empty($entity->pay_cycle) ? paycycle_cycle_opts($entity->pay_cycle) : t('ERROR'),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    $content[] = array(
      '#type' => 'item',
      '#field_name' => 'paycycle_pay_group',
      '#label' => t('Pay group'),
      '#label_display' => 'inline',
      '#markup' => !empty($entity->pay_group) ? paycycle_group_opts($entity->pay_group) : t('ERROR'),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
