<?php

/**
 * @file
 * Administration pages for Pay Cycle Reporting.
 *
 * @see paycycle.module
 */

/**
 * Page callback for the administration page.
 */
function paycycle_admin() {
  drupal_set_breadcrumb(array(l(t('Home'), '<front>'), l(t('Administration'), 'admin'), l(t('Structure'), 'admin/structure')));

  $result = db_query("SELECT paycycle_id, pay_cycle, pay_group, pay_date FROM {paycycle} ORDER BY pay_date DESC, pay_cycle DESC")->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $actions = array(
      l(t('edit'), 'admin/structure/paycycle/' . $item->paycycle_id . '/edit'),
      l(t('delete'), 'admin/structure/paycycle/' . $item->paycycle_id. '/delete'),
    );
    $rows[] = array(
      !empty($item->pay_date) ? format_date($item->pay_date, 'custom', 'n/j/Y') : t('ERROR'),
      !empty($item->pay_cycle) ? paycycle_cycle_opts($item->pay_cycle) : t('ERROR'),
      !empty($item->pay_group) ? paycycle_group_opts($item->pay_group) : t('ERROR'),
      implode(" ", $actions),
    );
  }
  if (!empty($rows)) {
    $limit = 30;
    $page = pager_default_initialize(count($rows), $limit, 0);
    $offset = $limit * $page;
    return array(
      array(
        '#theme' => 'table',
        '#header' => array(t('Pay date'), t('Pay cycle'), t('Pay group'), t('Actions')),
        '#rows' => array_slice($rows, $offset, $limit),
      ),
      array(
        '#theme' => 'pager',
      ),
    );
  }
  else {
    return array('#markup' => t('No items found.'));
  }
}

/**
 * Display a paycycle entry.
 */
function paycycle_page_view($entity, $view_mode = 'full') {
  return entity_view('paycycle', array($entity), $view_mode);
}

/**
 * Add new paycycle entry.
 */
function paycycle_add() {
  $entity = entity_create('paycycle', array());
  return drupal_get_form('paycycle_form', $entity);
}

/**
 * Edit form for paycycle entry.
 */
function paycycle_form($form, &$form_state, $entity) {
  // dpm($entity, 'entity');
  $form_title = t('Editing @title', array('@title' => $entity->label()));
  drupal_set_title(empty($entity->is_new) ? $form_title : t('Add new pay cycle'));

  $form = array();

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['pay_date'] = array(
    '#title' => t('Pay date'),
    '#type' => 'date_popup', // Provided by the date_popup module
    '#date_format' => 'D, n/j/Y', // Uses the PHP date() format - http://php.net/manual/en/function.date.php
    '#date_year_range' => '0:+2', // Limits the year range to the next two upcoming years
    '#required' => TRUE,
    '#default_value' => !empty($entity->pay_date) ? date('Y-m-d', $entity->pay_date) : NULL, // Default value must be in 'Y-m-d' format.
  );

  $form['pay_cycle'] = array(
    '#title' => t('Pay cycle'),
    '#type' => 'radios',
    '#options' => paycycle_cycle_opts(),
    '#required' => TRUE,
    '#default_value' => !empty($entity->pay_cycle) ? $entity->pay_cycle : NULL,
  );

  $form['pay_group'] = array(
    '#title' => t('Pay group'),
    '#type' => 'radios',
    '#options' => paycycle_group_opts(),
    '#required' => TRUE,
    '#default_value' => !empty($entity->pay_group) ? $entity->pay_group : NULL,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Pay cycle entry form submit.
 */
function paycycle_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  $entity->pay_cycle = $form_state['values']['pay_cycle'];
  $entity->pay_group = $form_state['values']['pay_group'];
  $pay_date = new DateTime($form_state['values']['pay_date']);
  $entity->pay_date = $pay_date->getTimestamp();
  entity_save('paycycle', $entity);
  $form_state['redirect'] = 'admin/structure/paycycle';
}

/*
 * Delete confirmation for pay cycle entry.
 */
function paycycle_delete_confirm($form, &$form_state, $entity) {
  // Always provide entity id in the same form key as in the entity edit form.
  $form['id'] = array('#type' => 'value', '#value' => $entity->paycycle_id);
  return confirm_form($form,
    t('Are you sure you want to delete @title?', array('@title' => $entity->label())),
    'admin/structure/paycycle/',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/*
 * Delete confirmation submission for pay cycle entry.
 */
function paycycle_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entity = entity_load('paycycle', array($form_state['values']['id']));
    $entity = reset($entity);
    entity_delete('paycycle', $entity->paycycle_id);
    cache_clear_all();
    watchdog('content', 'Deleted @title.', array('@title' => $entity->label()));
    drupal_set_message(t('@title has been deleted.', array('@title' => $entity->label())));
  }

  $form_state['redirect'] = 'admin/structure/paycycle';
}
